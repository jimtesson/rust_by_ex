/// this is documenting the following function method to add_one. To be used as follow:
/// assert_eq!(add_one(0),1);
pub fn add_one(x: i32) -> i32{
    x+1
}

pub fn say_hello(){
    println!("Hello, world!");
    println!("This is {0}, this is {1}. Again {0} and {1}","Blooby","Bluebi");
    println!("{subject} {verb} {object}", object="gelati", verb="mangiamo", subject="Noi");
    println!("{} is not equal to {:b} ", 1,3);
    // automatic printing structure with debug trait
    println!("{:?} is the value of myStruct",MyStruct(11));
    println!("{:?} is the value of the container of myStruct", MyStructContainer(MyStruct(12)));

    // pretty print of structure
    let name = "octopus";
    let age = 10;
    let octo = Person{name,age};
    println!("-> Nice structure print: {:#?}",octo);

    
}

#[derive(Debug)]
struct MyStruct(i32);

#[derive(Debug)]
struct MyStructContainer(MyStruct);

#[derive(Debug)]
struct Person<'a> {
    name: &'a str,
    age: u8
}

#[derive(Debug)]
struct Point2D{
    x: f64,
    y: f64,
}
use std::fmt;
impl fmt::Display for Point2D{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        write!(f,"x: {}, y: {}",self.x,self.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
    #[test]
    fn add_one_test(){
        assert_eq!(add_one(0),1);
    }
}
