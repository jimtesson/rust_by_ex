
pub struct Post{
    content: String,
}

pub struct DraftPost{
    content: String,
}

pub struct FirstPendingReviewPost{
    content: String,
}
pub struct SecondPendingReviewPost{
    content: String,
}


impl Post{
    pub fn new() -> DraftPost{
        DraftPost{
            content: String::new(),
        }
    }
    pub fn content(&self)->&str{
       &self.content
    }
}

impl DraftPost{
    pub fn add_text(&mut self, text: &str){
        self.content.push_str(text)
    }
    pub fn request_review(self)-> FirstPendingReviewPost{
        FirstPendingReviewPost{
            content: self.content, 
        }
    }
}
impl FirstPendingReviewPost{
    pub  fn approve(self)->SecondPendingReviewPost{
        SecondPendingReviewPost{
            content : self.content
        }
    }
    pub fn reject(self)->DraftPost{
        DraftPost{
            content : self.content
        }
    }
}
impl SecondPendingReviewPost{
    pub  fn approve(self)->Post{
        Post{
            content : self.content
        }
    }
    pub fn reject(self)->DraftPost{
        DraftPost{
            content : self.content
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut post = Post::new();

        post.add_text("Vivre sans blog: c'est possible.");

        let post = post.request_review(); // go to pendingReview
        
        let post = post.reject(); // go back to draft

        let post = post.request_review(); // go to pendingReview

        
        let post = post.approve();
        let post = post.approve(); // need two approve
        
        assert_eq!(post.content(),"Vivre sans blog: c'est possible.");
    }
}
