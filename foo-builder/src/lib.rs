pub mod foo_module {
    #[derive(Debug)]
    pub struct Foo{
        inner: u32,
    }

    pub struct FooBuilder{
        a: u32,
        b: u32,
    }

    impl FooBuilder{
        pub fn new(input: u32) -> Self{
            Self{
                a: input,
                b: input,
            }
        }
        pub fn double_a(self) -> Self{
            Self{
                a: self.a*2,
                b: self.b,
            }
        }
        pub fn get_foo(self) -> Foo{
            Foo{
                inner:self.a + self.b,
            }
        }
    }
}




#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        let x = foo_module::FooBuilder::new(10).double_a().get_foo();
        println!("{:#?}", x);
    }
}
