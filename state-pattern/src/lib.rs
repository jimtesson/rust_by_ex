
pub struct Post{
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post{
    pub fn new() -> Post{
        Post{
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }
    pub fn add_text(&mut self, text: &str){
        self.content.push_str(text);
    }
    pub fn content(&mut self)->&str{
       self.state.as_ref().unwrap().content(self)
    }
    pub fn request_review(&mut self){
       if let Some(s) = self.state.take(){
           self.state = Some(s.request_review())
       }
    }
    pub fn approve(&mut self){
        if let Some(s) = self.state.take(){
            self.state = Some(s.approve())
        }
    }
}

trait State{
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;
    fn content<'a>(&self, post: &'a Post) ->&'a str {
        ""
    }
}

struct Draft{}
struct PendingReview{}
struct Published{}

impl State for Draft{
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview{})
    }
    fn approve(self: Box<Self>) -> Box<dyn State>{
        self
    }
}
// Info: Box<Self>. This syntax means the method is only valid 
// when called on a Box holding the type. This syntax takes 
// ownership of Box<Self>, invalidating the old state so the 
// state value of the Post can transform into a new state.

impl State for PendingReview{
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }
    fn approve(self: Box<Self>) -> Box<dyn State>{
        Box::new(Published{})
    }
}

impl State for Published{
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }
    fn approve(self: Box<Self>) -> Box<dyn State>{
        self
    }
    fn content<'a>(&self, post: &'a Post) -> &'a str{
        &post.content
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut post = Post::new();

        post.add_text("bla bla bla is my draft");
        assert_eq!("",post.content());

        post.request_review();
        assert_eq!("",post.content());
        
        post.approve();
        assert_eq!("bla bla bla is my draft",post.content());
    
    }
}
