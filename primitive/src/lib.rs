
pub fn reverse_tupple(pair: (i32,bool)) -> (bool,i32){
    let (integer,boolean) = pair;
    println!("tupple ex: {:?}",pair);
    (boolean,integer)
}

#[derive(Debug)]
pub struct Matrix(i32,i32,i32,i32);

use std::fmt;
impl fmt::Display for Matrix{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        write!(f,"( {} {} ) /n ( {} {} )",self.0,self.1,self.2,self.3)
    }
}

pub fn transpose_matrix(matrix: Matrix) -> Matrix{
    Matrix(matrix.0,matrix.2,matrix.1,matrix.3)
}

pub fn print_matrix(pair: (i32,i32,i32,i32)){
   let mat = Matrix(pair.0,pair.1,pair.2,pair.3);
    println!("{}",mat);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn reverse_tupple_test(){
        let pair = (12,true);
        assert_eq!(reverse_tupple(pair),(true,12));
    }

    #[test]
    fn transpose_matrix_tst(){
        let matrix= Matrix(1,2,3,4);
        // assert_eq!(transpose_matrix(matrix),Matrix(1,3,2,4));
    }
}
