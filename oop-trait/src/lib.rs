// public trait that everyone can implement
pub trait Link{
    fn create_link(&self) -> Box<dyn GenericPath>;
}
pub trait GenericPath{
    fn get_path(&self) -> Vec<i32>;
}

pub struct Line{
    pub x: i32,
    pub y: i32,
}
impl GenericPath for Line{
    fn get_path(&self) -> Vec<i32>{
        let my_path = vec![self.x,self.y];
        println!("<--> line : {:?}",my_path);
        my_path 
    }
}

pub struct Ring{
    pub xx: i32,
    pub yy: i32,
}
impl GenericPath for Ring{
    fn get_path(&self) -> Vec<i32>{
        let my_path = vec![self.xx,self.yy,self.xx];
        println!("<--> ring : {:?}",my_path);
        my_path
    }
}

// summitLinker
#[derive(Clone)]
pub struct SummitLinker{
    pub data: String,
    pub summit1: String,
    pub summit2: String,
}
impl Link for SummitLinker{
    fn create_link(&self) -> Box<dyn GenericPath>{
        println!("-> summit linking:");
        let link = Line{x:1,y:2};
        Box::new(link)
    }
}
// DirectLinker
#[derive(Clone)]
pub struct DirectLinker{
    pub data: String,
    pub line1: String,
    pub line2: String,
}
impl Link for DirectLinker{
    fn create_link(&self) -> Box<dyn GenericPath>{
        println!("-> Direct linking:");
        let link = Ring{xx:1,yy:2};
        Box::new(link)}
}

// the user
pub struct PathMaker{
    pub linker1: Box<dyn Link>,
    pub linker2: Box<dyn Link>,
} 

impl PathMaker{
    pub fn create_path_for_mortise(&self) -> Box<dyn GenericPath>{
        println!(" Creating Mortise path");
        let path = self.linker1.create_link();
        path
    }
    pub fn create_path_for_tenon(&self) -> Box<dyn GenericPath>{
        println!("Creating Tenon path");
        let path = self.linker2.create_link();
        path
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_path_test(){
        let my_pathmaker = PathMaker{
            linker1: Box::new(SummitLinker{
                data: String::from("a list of summit ...."),
                summit1: String::from("10,50"),
                summit2: String::from("20,30"),
                }),
            linker2: Box::new(DirectLinker{
            data: String::from("a list of lines ..."),
            line1: String::from("10,50"),
            line2: String::from("20,30"),
            }),
        };
        my_pathmaker.create_path_for_mortise().get_path();
        my_pathmaker.create_path_for_tenon().get_path();
    }

}
