pub struct Stm32f30xPeripherals;

pub struct GpioConfig<ENABLED, DIRECTION, MODE> {
    periph: Stm32f30xPeripherals,
    enabled: ENABLED,
    direction: DIRECTION,
    mode: MODE,
}

// Type states for MODE in GPioConfig
pub struct Disabled;
pub struct Enabled;
pub struct Output;
pub struct Input;
pub struct PulledLow;
pub struct PulledHigh;
pub struct HighZ;
pub struct DontCare;

// function that can be used on any gpio pin
impl<EN,DIR,IN_MODE> GpioConfig<EN,DIR,IN_MODE> {
    pub fn into_disabled(self) -> GpioConfig<Disabled,DontCare,DontCare> {
       // self.periph.modify(|_r, w| w.enable.disabled());
        println!("pin is now disabled");
       GpioConfig{
           periph:self.periph,
           enabled: Disabled,
           direction: DontCare,
           mode: DontCare,
       }
    }

    pub fn into_enabled_input(self) -> GpioConfig<Enabled,Input,HighZ>{
        //  self.periph.modify(|_r, w| {
            // w.enable.enabled()
            //  .direction.input()
            //  .input_mode.high_z()
            println!("pin input is now enabled");
            GpioConfig{
                periph:self.periph,
                enabled:Enabled,
                direction: Input,
                mode: HighZ,
            }
        // });
    }
    pub fn into_enabled_output(self) -> GpioConfig<Enabled,Output,DontCare>{
            // self.periph.modify(|_r, w| {
            // w.enable.enabled()
            //  .direction.output()
            //  .input_mode.set_high()
            println!("pin ouput is now enabled");
            GpioConfig{
                periph: self.periph,
                enabled: Enabled,
                direction: Output,
                mode: DontCare,
            }
    }
}

// function that can be used only on output enabled pin
impl GpioConfig<Enabled,Output,DontCare> {
    
    pub fn set_bit(&mut self, set_high: bool){
        // self.periph.modify(|_r, w| w.output_mode.set_bit(set_high));
        println!("set bit:{}", set_high);
    }
}

// methods that can be used on any enabled input GPIO
impl<IN_MODE> GpioConfig<Enabled,Input, IN_MODE>{
    
    pub fn bit_is_set(&self) -> bool {
        // self.periph.read().input_status.bit_is_set()
        println!("input is:{}", true);
        true
    }

    pub fn into_input_high_z(self) -> GpioConfig<Enabled,Input,HighZ> {
        // self.periph.modify(|_r, w| w.input_mode().high_z());
        println!("set input high z");
        GpioConfig {
            periph: self.periph,
            enabled: Enabled,
            direction: Input,
            mode: HighZ,
        }
    }

    pub fn into_input_pull_down(self) -> GpioConfig<Enabled,Input,PulledLow> {
        // self.periph.modify(|_r, w| w.input_mode().pull_low());
        println!("set input pull down");
        GpioConfig {
            periph: self.periph,
            enabled: Enabled,
            direction: Input,
            mode: PulledLow,
        }
    }
    pub fn into_input_pull_up(self) -> GpioConfig<Enabled,Input,PulledHigh> {
        // self.periph.modify(|_r, w| w.input_mode().pull_low());
        println!("set input pull down");
        GpioConfig {
            periph: self.periph,
            enabled: Enabled,
            direction: Input,
            mode: PulledHigh,
        }
    }
}
pub struct GpioBuilder;
impl GpioBuilder{
    pub fn new() -> Self{Self{}}
    pub fn get_gpio(self,pin: u32) -> GpioConfig<Disabled,Input,HighZ> {
        let my_periph = Stm32f30xPeripherals{};
        println!("get perif from pin {}",pin);
        GpioConfig{
            periph: my_periph,
            enabled:Disabled,
            direction: Input,
            mode:HighZ,
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        // let x = foo_module::FooBuilder::new(10).double_a().get_foo();
        println!(" TEST GPIO...");
        /*
        * Example 1: Unconfigured to High-Z input
        */
        let pin: GpioConfig<Disabled, _, _> = GpioBuilder::new().get_gpio(10);
        
        // Can't do this, pin isn't enabled!
        // pin.into_input_pull_down();

        // Now turn the pin from unconfigured to a high-z input
        let input_pin = pin.into_enabled_input();

        // Read from the pin
        let pin_state = input_pin.bit_is_set();

        // Can't do this, input pins don't have this interface!
        // input_pin.set_bit(true);

        /*
         * Example 2: High-Z input to Pulled Low input
         */
        let pulled_low = input_pin.into_input_pull_down();
        let pin_state = pulled_low.bit_is_set();

        /*
        * Example 3: Pulled Low input to Output, set high
        */
        let mut output_pin = pulled_low.into_enabled_output();
        output_pin.set_bit(true);

        // Can't do this, output pins don't have this interface!
        // output_pin.into_input_pull_down();
    }
}